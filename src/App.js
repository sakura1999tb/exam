import logo from "./logo.svg";
import "./App.css";
import { useEffect, useState } from "react";
import { Button, Container, Form, InputGroup, Table } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";

const questions = [
  {
    title: "Tell me about yourself",
    index: 1,
  },
  {
    title: "Your greatest strength and weakness",
    index: 2,
  },
  {
    title: "Why did you choose Uniqlo?",
    index: 3,
  },
  {
    title: "See your self in the next 5 years?",
    index: 4,
  },
  {
    title: "Why leave your current job?( ensure your motivation)",
    index: 5,
  },
  {
    title: "What do you  can contribute the company?",
    index: 6,
  },
  {
    title: "What do you can contribute for Uniqlo?",
    index: 7,
  },
  {
    title: "Your biggest achivement ",
    index: 8,
  },
  {
    title: "What qualities that a good manger should have?",
    index: 9,
  },
  {
    title: "Share the leadership you have in unversity/ work period?",
    index: 10,
  },
  {
    title: "What challeng you face within retail enviroment?",
    index: 11,
  },
  {
    title:
      "What was difficult challeng you have to face within retail enviroment?",
    index: 12,
  },
  {
    title:
      "What do you think makes the best store? How was a experice when shopping at Uniqlo and how can it be improve?",
    index: 13,
  },
  {
    title: "What is your goals ? what strategy to achieve the goal?",
    index: 14,
  },
  {
    title: "Why should we choose you?",
    index: 15,
  },
  {
    title: "What did you like most about your last position?",
    index: 16,
  },
  {
    index: 17,
    title: "What can you bring to the company",
  },
  {
    title: "What is your motivation?",
    index: 18,
  },
  {
    title: "What do you know about Uniqlo?",
    index: 19,
  },
  {
    title: "What diffirence between Uniqlo and other competitors?",
    index: 20,
  },
  {
    title: "How was you store experience day?",
    index: 21,
  },
  {
    title: "What the most impress in the information session?",
    index: 22,
  },
  {
    title: "What do you understand of lifewear?",
    index: 23,
  },
  {
    title: "Are you flexible to work oversee and aross country?",
    index: 24,
  },
  {
    title: "What are some of challengess people might face in this position",
    index: 25,
  },
  {
    title: "Who are UMC, can you decribe them in 3 words?",
    index: 26,
  },
  {
    title: "What is the biggest failture?",
    index: 27,
  },
  {
    title: "Disscus a lit bit about key compentencey?",
    index: 28,
  },
  {
    title: "How you can contribute for company?",
    index: 29,
  },
  {
    title: "If having any conflict, how do you handle with it?",
    index: 30,
  },
  {
    title: "Do you know leader responsibilities?",
    index: 31,
  },
  {
    title: "How the store do you want to contribute?",
    index: 32,
  },
  {
    title: "What kind of enviroment do you want to work for?",
    index: 33,
  },
  {
    title: "What challenge do you expect when you become UMC?",
    index: 34,
  },
  {
    title:
      "What is the biggest challenge and how did you overcome this situation?",
    index: 35,
  },
  {
    title:
      "How did you engage your team and thw biggest challenge when involving divese people?",
    index: 36,
  },
  {
    title: "What did you do at university?",
    index: 37,
  },
  {
    title: "Do you know about: Heatech/Airism/Lightdown?",
    index: 38,
  },
  {
    title: "Tell me more about previous job.",
    index: 39,
  },
  {
    title:
      "Enhancement of high standards customer experience in store is a crucial factor for the development of UNIQLO. What actions will you suggest to improve the above elements if you are the UMC?",
    index: 40,
  },
  {
    title:
      "Describe your ultimate career goal in your life & how would you want to take advantage of UMC position in order to achieve it?",
    index: 41,
  },
];

const stateQuestions = {
  READY: "READY",
  OCCURING: "OCCURING",
  END: "END",
};

const numtoTime = (num) => {
  const seconds =
    (num / 1000) % 60 < 10 ? `0${(num / 1000) % 60}` : (num / 1000) % 60;

  const min =
    (num / 1000 - seconds) / 60 < 10
      ? `0${(num / 1000 - seconds) / 60}`
      : (num / 1000 - seconds) / 60;

  return `${min}:${seconds}`;
};

let timeout = null;

function App() {
  const [currentQuestionIndex, setCurrentQuestionIndex] = useState(1);
  const [totalTime, setTotalTime] = useState(3);
  const [time, setTime] = useState(3 * 1000 * 60);
  const [preQuestions, setPreQuestions] = useState([]);
  const [stateQuestion, setStateQuestion] = useState("READY");
  const [preStateQuestion, setPreStateQuestion] = useState("READY");
  const [newTotalTime, setNewTotalTime] = useState(totalTime);
  const [isEdit, setIsEdit] = useState(false);
  const [history, setHistory] = useState(
    JSON.parse(localStorage.getItem("history") || "[]")
  );
  const [currentHistory, setCurrentHistory] = useState({});

  useEffect(() => {
    if (
      stateQuestion === stateQuestions.OCCURING &&
      preStateQuestion === stateQuestions.READY
    ) {
      timeout = setTimeout(() => {
        if (time - 1000 >= 0) {
          setTime(time - 1000);
        } else if (time === 0) {
          clearTimeout(timeout);
          setStateQuestion(stateQuestions.END);
          setPreStateQuestion(stateQuestions.OCCURING);
        }
      }, 1000);
    }
  }, [stateQuestion, preStateQuestion, time]);

  const setNewQuestion = () => {
    setPreQuestions([...preQuestions, currentQuestionIndex]);
    const restQuestions =
      [...preQuestions, currentQuestionIndex].length === questions.length
        ? [...questions]
        : [...questions].filter(
            (question) =>
              ![...preQuestions, currentQuestionIndex].includes(question.index)
          );
    console.log("restQuestions", restQuestions);
    const newCurrentQuestionIndex =
      restQuestions[
        Math.floor(Math.random() * (restQuestions.length - 1 - 0 + 1) + 0)
      ].index;
    setCurrentQuestionIndex(newCurrentQuestionIndex);
    if (restQuestions.length === questions.length) {
      setPreQuestions([]);
    }
  };

  const pressStart = () => {
    setTime(totalTime * 60 * 1000);
    clearInterval(timeout);
    setStateQuestion(stateQuestions.OCCURING);
    setPreStateQuestion(stateQuestions.READY);
    setCurrentHistory({ start: Date.now(), question: currentQuestionIndex });
  };

  const pressRestart = () => {
    clearInterval(timeout);
    setStateQuestion(stateQuestions.READY);
    setPreStateQuestion(stateQuestions.OCCURING);
    setTime(totalTime * 60 * 1000);
    setCurrentHistory({});
  };

  const pressSubmit = () => {
    const duration = totalTime * 60 * 1000 - time;
    setCurrentHistory({});
    setHistory([{ ...currentHistory, duration }, ...history]);
    localStorage.setItem(
      "history",
      JSON.stringify([{ ...currentHistory, duration }, ...history])
    );
    setStateQuestion(stateQuestions.END);
    setPreStateQuestion(stateQuestions.OCCURING);
    setTime(totalTime * 60 * 1000);
    clearInterval(timeout);
  };

  const pressClearHistory = () => {
    if (stateQuestion === stateQuestions.READY) {
      setHistory([]);
      localStorage.setItem("history", JSON.stringify([]));
    }
  };

  const renderButton = () => {
    switch (stateQuestion) {
      case stateQuestions.READY:
        return (
          <>
            <Button
              variant="primary"
              onClick={() => {
                pressStart();
              }}
              style={{ marginLeft: "15px", marginRight: "15px" }}
            >
              Start
            </Button>
            <Button
              variant="primary"
              onClick={() => {
                setTime(totalTime * 60 * 1000);
                setNewQuestion();
              }}
              style={{ marginLeft: "15px", marginRight: "15px" }}
            >
              Other Question
            </Button>
          </>
        );
      case stateQuestions.OCCURING:
        return (
          <>
            <Button
              onClick={pressRestart}
              style={{ marginLeft: "15px", marginRight: "15px" }}
            >
              Restart
            </Button>
            <Button
              onClick={pressSubmit}
              style={{ marginLeft: "15px", marginRight: "15px" }}
            >
              Submit
            </Button>
          </>
        );
      case stateQuestions.END:
        return (
          <Button
            onClick={() => {
              setTime(totalTime * 60 * 1000);
              setNewQuestion();
              setStateQuestion(stateQuestions.READY);
              setPreStateQuestion(stateQuestions.READY);
            }}
          >
            Other Question
          </Button>
        );
      default:
        break;
    }
  };

  const currentQuestion = questions.find(
    (question) => question.index === currentQuestionIndex
  );
  console.log("currentQuestionIndex", currentQuestionIndex, currentQuestion);
  console.log("stateQuestion", stateQuestion);
  return (
    <Container fluid>
      <div
        className="d-flex"
        style={{
          marginTop: "200px",
          justifyContent: "space-between",
          paddingLeft: "100px",
          paddingRight: "100px",
        }}
      >
        <div style={{ flex: 2 }}></div>
        <div style={{ flex: 3 }}>
          <div style={{ height: "100px" }}>
            <h4>
              {currentQuestion.index}. {currentQuestion.title}
            </h4>
          </div>
          <div
            style={{
              marginTop: "380px",
            }}
          >
            {stateQuestion === stateQuestions.END && (
              <h4
                style={{
                  display: "flex",
                  justifyContent: "center",
                }}
              >
                End Time !!!!!!!!!
              </h4>
            )}
            <div
              style={{
                display: "flex",
                justifyContent: "center",
              }}
            >
              {renderButton()}
            </div>
          </div>
        </div>
        <div style={{ flex: 2, marginLeft: "40px" }}>
          <InputGroup className="mb-2" size="md">
            <InputGroup.Text id="basic-addon3">Set total time:</InputGroup.Text>
            <Form.Control
              className="w-50"
              id="basic-url"
              aria-describedby="basic-addon3"
              placeholder="min"
              disabled={!isEdit}
              onChange={(e) => {
                console.log("stateQuestion", stateQuestion, e.target.value);
                if (stateQuestion === stateQuestions.READY) {
                  const totalTime = !isNaN(parseFloat(e.target.value))
                    ? parseFloat(e.target.value)
                    : 3;
                  setNewTotalTime(totalTime);
                }
              }}
              value={isEdit ? newTotalTime : totalTime}
            />
            <Button
              variant="outline-secondary"
              id="button-addon1"
              onClick={() => {
                if (stateQuestion === stateQuestions.READY) {
                  if (isEdit) {
                    setTotalTime(newTotalTime);
                    setTime(newTotalTime * 60 * 1000);
                    setIsEdit(false);
                  } else {
                    setIsEdit(true);
                  }
                }
              }}
            >
              {isEdit ? "Save" : "Edit"}
            </Button>
          </InputGroup>
          <div
            style={{
              display: "flex",
              marginTop: "30px",
              justifyContent: "space-between",
            }}
          >
            <div style={{ display: "flex" }}>
              <h3> Time: </h3>
              <h3 style={{ marginLeft: "20px" }}>{numtoTime(time)}</h3>
            </div>
            <Button variant={"secondary"} onClick={pressClearHistory}>
              Clear History
            </Button>
          </div>
          <div
            style={{ height: "500px", overflowY: "auto", marginTop: "30px" }}
          >
            <Table striped bordered hover responsive>
              <thead>
                <tr>
                  <th>#</th>
                  <th>Start At</th>
                  <th>Question</th>
                  <th>Duration</th>
                </tr>
              </thead>
              <tbody>
                {Array.isArray(history) &&
                  history.map(({ start, question, duration }, index) => {
                    return (
                      <tr>
                        <td>{index}</td>
                        <td>{new Date(start).toLocaleString()}</td>
                        <td>{question}</td>
                        <td>{numtoTime(duration)}</td>
                      </tr>
                    );
                  })}
              </tbody>
            </Table>
          </div>
        </div>
      </div>
    </Container>
  );
}

export default App;
